ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8-minimal
ARG BASE_TAG=8.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

COPY tflint_linux_amd64.zip /

RUN microdnf update --nodocs && \
    microdnf install -y --nodocs shadow-utils unzip && \
    groupadd -r tflint && useradd --no-log-init -r -g tflint tflint && \
    mkdir -p /home/tflint && \
    chown tflint:tflint /home/tflint && \
    unzip /tflint_linux_amd64.zip -d /usr/local/bin && \
    chmod +x /usr/local/bin/tflint && \
    rm -f /tflint_linux_amd64.zip && \
    microdnf remove libsemanage shadow-utils unzip && \
    microdnf clean all && \
    rm -rf /var/yum/cache

USER tflint
HEALTHCHECK NONE
ENTRYPOINT ["tflint"]
