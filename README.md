# TFLint

TFLint is a linter for Terraform code. Out of the box it has several rules as well as bundled AWS rules.
Plugins for the other major clouds are available and configurable.

```md
TFLint is a framework and each feature is provided by plugins, the key features are as follows:

- Find possible errors (like illegal instance types) for Major Cloud providers (AWS/Azure/GCP).
- Warn about deprecated syntax, unused declarations.
- Enforce best practices, naming conventions.
```

Upstream documentation can be found [here](https://github.com/terraform-linters/tflint).
